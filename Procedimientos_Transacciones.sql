use master
go
use Viaje_Placentero
go

--Procedimientos para dar de alta en la BD **************************************************************************************************************************
-- VIAJES

DROP PROCEDURE IF EXISTS  SP_AGREGAR_NUEVO_VIAJE
GO
create procedure SP_AGREGAR_NUEVO_VIAJE(
	@EstOrigen varchar(30),
	@CdOrigen varchar(30),
	@EstDestino varchar(30),
	@CdDestino varchar(30),
	@Fecha_Partida date,
	@HoraPartida time
) as
begin
	begin transaction
		insert into Viaje(EstadoOrigen,CiudadOrigen,EstadoDestino,CiudadDestino,Fecha,Hora_Partida)
		values (@EstOrigen,@CdOrigen,@EstDestino,@CdDestino,@Fecha_Partida,@HoraPartida)
		DECLARE @Error int
		SET @Error=@@ERROR
		IF (@Error<>0) ROLLBACK TRAN 
	commit transaction
end
go

-- Dar de alta a un agencia
DROP PROCEDURE IF EXISTS SP_ADD_AGENCIA
GO
CREATE PROCEDURE SP_ADD_AGENCIA(
	@Nombre varchar(30),
	@Calle varchar(30),
	@Colonia varchar(30),
	@Codigo_postal varchar(15),
	@Ciudad varchar(30),
	@Estado varchar(30),
	@Numero varchar(15)
	) AS
	BEGIN	
		BEGIN TRANSACTION 
			INSERT INTO Agencia(Nombre_Agencia,Calle,Colonia,Codigo_postal,Ciudad,Estado,Numero)
			VALUES (@Nombre,@Calle,@Colonia,@Codigo_postal,@Ciudad,@Estado,@Numero)
			DECLARE @Error int
			SET @Error=@@ERROR
			IF (@Error<>0) ROLLBACK TRAN 
		COMMIT TRANSACTION
	END
GO

--EMPLEADO
DROP PROCEDURE IF EXISTS SP_ADD_EMPLEADO
GO
CREATE PROCEDURE SP_ADD_EMPLEADO(
	@Nombre varchar(40),
	@Apellidos VARCHAR(40),
	@Tipo_Empleado int, --f
	@Id_Agencia int --f
	) AS
	BEGIN
		BEGIN TRANSACTION 
			INSERT INTO Empleado(Nombre,Apellidos,Tipo_Empleado,Id_Agencia) VALUES(@Nombre,@Apellidos, (SELECT Id_Tipo FROM TipoEmpleado WHERE Id_Tipo=@Tipo_Empleado),(SELECT Id_Agencia FROM Agencia WHERE Id_Agencia=@Id_Agencia))
		COMMIT TRANSACTION
	END
GO

--Tripulacion

--Tipo_Viaje
DROP PROCEDURE IF EXISTS SP_ADD_TIPO_VIAJE
GO
CREATE PROCEDURE SP_ADD_TIPO_VIAJE(
	@TipoViaje varchar(5)
	)AS
	BEGIN
		BEGIN TRANSACTION
			INSERT INTO Tipo_Viaje(Tipo) VALUES(@TipoViaje)
			DECLARE @Error int
			SET @Error=@@ERROR
			IF (@Error<>0) ROLLBACK TRAN 
		COMMIT TRANSACTION
	END
GO

-- TipoAutobus
DROP PROCEDURE IF EXISTS ADD_TIPO_AUTOBUS
GO
CREATE PROCEDURE ADD_TIPO_AUTOBUS (
	@Clase_Camion VARCHAR(20)
)AS
	BEGIN
		BEGIN TRANSACTION
			INSERT INTO TipoAutobus(Clase_Camion) VALUES(@Clase_Camion)
			DECLARE @Error int
			SET @Error=@@ERROR
			IF (@Error<>0) ROLLBACK TRAN 
		COMMIT TRANSACTION
	END
GO

--Autobus
DROP PROCEDURE IF EXISTS ADD_AUTOBUS
GO
CREATE PROCEDURE ADD_AUTOBUS(
	@Marca varchar(30),
	@AsientosDisp int,
	@Capacidad int,
	@Progra varchar(2),
	@Tipo int --foraneo
)AS
	BEGIN
		BEGIN TRANSACTION
			INSERT INTO Autobus(Marca,AsientosDisponibles,Capacidad,Programable,Tipo_Autobus) 
			VALUES(@Marca,@AsientosDisp,@Capacidad,@Progra, (SELECT Id_Tipo FROM TipoAutobus WHERE Id_Tipo=@Tipo))
			DECLARE @Error int
			SET @Error=@@ERROR
			IF (@Error<>0) ROLLBACK TRAN
		COMMIT TRANSACTION
	END
GO

-- Equipaje
DROP PROCEDURE IF EXISTS ADD_EQUIPAJE
GO
CREATE PROCEDURE ADD_EQUIPAJE (
	@Peso float,
	@CostoXKgAdicional money 
)AS
	BEGIN
		BEGIN TRANSACTION
			INSERT INTO Equipaje(Peso,CostoXKgAdicional) VALUES(@Peso,@CostoXKgAdicional)
			DECLARE @Error int
			SET @Error=@@ERROR
			IF (@Error<>0) ROLLBACK TRAN 
		COMMIT TRANSACTION
	END
GO

-- Pasajero
DROP PROCEDURE IF EXISTS ADD_PASAJERO
GO
CREATE PROCEDURE ADD_PASAJERO(
	@Nombre varchar(40),
	@Apellidos varchar(40),
	@FechaNac date,
	@Calle varchar(30),
	@Colonia varchar(30),
	@CP varchar(15),
	@Ciudad varchar(30),
	@Estado varchar(30),
	@Num varchar(15),
	@Tel varchar(10),
	@DNI varchar(18),
	@RFC_Comprador varchar (13),
	@Id_Equipaje int --FORANEO
)AS
	BEGIN
		BEGIN TRANSACTION
			INSERT INTO Pasajero(Nombre,Apellidos,FechaNacimiento,Calle,Colonia,Codigo_Postal,Ciudad,Estado,Numero,Telefono,DNI,RFC_Comprador,Id_Equipaje) 
			VALUES(@Nombre,@Apellidos, @FechaNac, @Calle, @Colonia, @CP, @Ciudad, @Estado, @Num, @Tel, @DNI, @RFC_Comprador, (SELECT Id_Equipaje FROM Equipaje WHERE Id_Equipaje=@Id_Equipaje))
			DECLARE @Error int
			SET @Error=@@ERROR
			IF (@Error<>0) ROLLBACK TRAN
		COMMIT TRANSACTION
	END
GO

-- Boleto
DROP PROCEDURE IF EXISTS ADD_BOLETO
GO
CREATE PROCEDURE ADD_BOLETO(
	@Fecha_Emision date,
	@Costo money,
	@No_Asiento int,
	@Reserva varchar(2),
	@Id_Empleado int,-- f
	@Id_Viaje int, --f
	@Id_Pasajero int --f
)AS
	BEGIN
		BEGIN TRANSACTION
			INSERT INTO Boleto(Fecha_Emision,Costo,No_Asiento,Reserva,Id_Empleado,Id_Viaje,Id_Pasajero) 
			VALUES(@Fecha_Emision,@Costo,@No_Asiento,@Reserva,(SELECT Id_Empleado FROM Empleado WHERE Id_Empleado=@Id_Empleado),
			(SELECT Id_Viaje FROM Viaje WHERE Id_Viaje=@Id_Viaje),(SELECT Id_Pasajero FROM Pasajero WHERE Id_Pasajero=@Id_Pasajero))
			DECLARE @Error int
			SET @Error=@@ERROR
			IF (@Error<>0) ROLLBACK TRAN 
		COMMIT TRANSACTION
	END
GO

-- Remitente
DROP PROCEDURE IF EXISTS ADD_REMITENTE
GO
CREATE PROCEDURE ADD_REMITENTE(
	@Nombre varchar(40),
	@Apellidos varchar(40),
	@Ciudad varchar(25),
	@Razon_Social varchar(40),
	@Direccion varchar(50)

)AS
	BEGIN
		BEGIN TRANSACTION
			INSERT INTO Remitente(Nombre,Apellidos,Ciudad,Razon_Social,Direccion) VALUES(@Nombre,@Apellidos,@Ciudad,@Razon_Social,@Direccion)
			DECLARE @Error int
			SET @Error=@@ERROR
			IF (@Error<>0) ROLLBACK TRAN 
		COMMIT TRANSACTION
	END
GO

-- Destinatario
DROP PROCEDURE IF EXISTS ADD_DESTINATARIO
GO
CREATE PROCEDURE ADD_DESTINATARIO(
	@Nombre varchar(40),
	@Apellidos varchar(40),
	@Ciudad varchar(25),
	@Direccion varchar(50)

)AS
	BEGIN
		BEGIN TRANSACTION
			INSERT INTO Destinatario(Nombre,Apellidos,Ciudad,Direccion) VALUES(@Nombre,@Apellidos,@Ciudad,@Direccion)
			DECLARE @Error int
			SET @Error=@@ERROR
			IF (@Error<>0) ROLLBACK TRAN 
		COMMIT TRANSACTION
	END
GO
-- Comprobante
DROP PROCEDURE IF EXISTS ADD_COMPROBANTE
GO
CREATE PROCEDURE ADD_COMPROBANTE (
	@FK_Remitente int,
	@FK_Destinatario int
)AS
	BEGIN
		BEGIN TRANSACTION
			INSERT INTO Comprobante(FK_Remitente,FK_Destinatario) VALUES((SELECT Id_Remitente FROM Remitente WHERE Id_Remitente=@FK_Remitente),(SELECT Id_Destinatario FROM Destinatario WHERE Id_Destinatario=@FK_Destinatario))
			DECLARE @Error int
			SET @Error=@@ERROR
			IF (@Error<>0) ROLLBACK TRAN 
		COMMIT TRANSACTION
	END
GO

-- Factura
DROP PROCEDURE IF EXISTS ADD_FACTURA
GO
CREATE PROCEDURE ADD_FACTURA (
	@UsoCFDI varchar(30),
	@FK_Comprobante int,
	@FK_Boleto int
)AS
	BEGIN
		BEGIN TRANSACTION
			INSERT INTO Factura(UsoCFDI,FK_Comprobante,FK_Boleto) VALUES(@UsoCFDI, (SELECT Id_Comprobante FROM Comprobante WHERE Id_Comprobante=@FK_Comprobante),(SELECT Id_Boleto FROM Boleto WHERE Id_Boleto=@FK_Boleto))
			DECLARE @Error int
			SET @Error=@@ERROR
			IF (@Error<>0) ROLLBACK TRAN 
		COMMIT TRANSACTION
	END
GO

--CamionCarga
DROP PROCEDURE IF EXISTS ADD_CAMIONCARGA
GO
CREATE PROCEDURE ADD_CAMIONCARGA (
	@Marca varchar(25),
	@Capacidad int
)AS
	BEGIN
		BEGIN TRANSACTION
			INSERT INTO CamionCarga(Marca,Capacidad) VALUES(@Marca,@Capacidad)
			DECLARE @Error int
			SET @Error=@@ERROR
			IF (@Error<>0) ROLLBACK TRAN
		COMMIT TRANSACTION
	END
GO

--Paquete
DROP PROCEDURE IF EXISTS ADD_PAQUETE
GO
CREATE PROCEDURE ADD_PAQUETE(
	@Peso float,
	@Tipo_Servicio varchar(10),
	@TransporteCarga int, --F
	@Autobus int, --F
	@Comprobante int --F
)AS
	BEGIN
		BEGIN TRANSACTION
			INSERT INTO Paquete(Peso,Tipo_Servicio,FK_TransporteCarga,FK_Autobus,FK_Comprobante) VALUES(@Peso, @Tipo_Servicio,(SELECT Id_Camion FROM CamionCarga WHERE Id_Camion=@TransporteCarga),
			(SELECT Id_Autobus FROM Autobus WHERE Id_Autobus=@Autobus), (SELECT Id_Comprobante FROM Comprobante WHERE Id_Comprobante=@Comprobante))
			DECLARE @Error int
			SET @Error=@@ERROR
			IF (@Error<>0) ROLLBACK TRAN 
		COMMIT TRANSACTION
	END
GO

-- Sobre
DROP PROCEDURE IF EXISTS ADD_SOBRE
GO
CREATE PROCEDURE ADD_SOBRE(
	@Tipo_Servicio varchar(10),
	@TransporteCarga int,
	@Comprobante int
)AS
	BEGIN
		BEGIN TRANSACTION
			INSERT INTO Sobre(Tipo_Servicio,FK_TransporteCarga,FK_Comprobante) VALUES(@Tipo_Servicio, (SELECT Id_Camion FROM CamionCarga WHERE Id_Camion=@TransporteCarga),
			(SELECT Id_Comprobante FROM Comprobante WHERE Id_Comprobante=@Comprobante))
			DECLARE @Error int
			SET @Error=@@ERROR
			IF (@Error<>0) ROLLBACK TRAN 
		COMMIT TRANSACTION
	END
GO
-- .................................

EXEC ADD_TIPO_AUTOBUS @Clase_Camion = 'Primera Clase'
SELECT * FROM TipoAutobus

EXEC ADD_AUTOBUS @Marca = 'ford', @AsientosDisp = 10, @Capacidad = 80, @Progra = 'SI', @Tipo = 2
SELECT * FROM Autobus

EXEC SP_ADD_AGENCIA @Nombre = 'Agencia Vuelos Felices', @Calle = 'La Chona', @Colonia = 'Palacio de Hierro', @Codigo_postal = '28076', @Ciudad = 'Colima',
	@Estado = 'Manzanillo', @Numero = 155
SELECT * FROM Agencia

--Procedimientos para consultas ************************************************************************************************************************************
DROP PROCEDURE IF EXISTS SP_DESPLEGAR_TRIPULACIONES_DISPONIBLES
GO
create procedure SP_DESPLEGAR_TRIPULACIONES_DISPONIBLES(
@FechaViaje date
) as 
begin
	declare  @Viajes_Largos table(Id_Viaje int)
	insert into @Viajes_Largos select V.Id_Viaje from Viaje as V inner join Tipo_Viaje  as TP on V.Tipo_Viaje=TP.Id_Tipo where TP.Tipo='Largo'

	declare @Viajes_Cortos table(Id_Viaje int)
	insert into @Viajes_Cortos select V.Id_Viaje from Viaje as V inner join Tipo_Viaje  as TP on V.Tipo_Viaje=TP.Id_Tipo where TP.Tipo='Corto'

	--declare @FechaViaje date

	select T.Id_Tripulacion from Tripulacion as T inner join Trip_Viaje as TV on T.Id_Tripulacion=TV.Id_Tripulacion inner join Viaje as V on 
	TV.Id_Viaje=V.Id_Viaje where (DATEPART(DAY,@FechaViaje) - DATEPART(DAY,V.Fecha))>=1 and V.Id_Viaje in (select Id_Viaje from @Viajes_Cortos) 
	intersect select T.Id_Tripulacion from Tripulacion as T inner join Trip_Viaje as TV on T.Id_Tripulacion=TV.Id_Tripulacion inner join Viaje as V on TV.Id_Viaje=V.Id_Viaje 
	where (DATEPART(DAY,@FechaViaje) - DATEPART(DAY,V.Fecha))>=2 and V.Id_Viaje in (select Id_Viaje from @Viajes_Largos)
end
GO
